/*
 * @Author: yenible
 * @Date: 2021-01-05 15:45:35
 * @LastEditors: yenible
 * @LastEditTime: 2021-02-28 17:04:19
 * @Description: xxx
 */
module.exports = {
  "presets": [
    "@vue/app",
    ["es2015", { "modules": false }]
  ],
  "plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
}