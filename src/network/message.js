/*
 * @Author: yenible
 * @Date: 2021-04-05 19:19:43
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-18 16:05:52
 * @Description: xxx
 */

import qs from "qs";

import {
  request
} from './request'



export function get_message_comment(data) {
  return request({
    url: 'message/get_message_comment',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
 
export function delete_comment_message(data) {
  return request({
    url: 'message/delete_comment_message',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
 
export function get_message_like(data) {
  return request({
    url: 'message/get_message_like',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
 
export function get_message_attract(data) {
  return request({
    url: 'message/get_message_attract',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}

export function delete_attract_message(data) {
  return request({
    url: 'message/delete_attract_message',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
export function delete_like_message(data) {
  return request({
    url: 'message/delete_like_message',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
export function read_all_comment(data) {
  return request({
    url: 'message/read_all_comment',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
export function read_all_like(data) {
  return request({
    url: 'message/read_all_like',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}
   export function read_all_attract(data) {
    return request({
      url: 'message/read_all_attract',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
  
      },
      data: qs.stringify(data)
    })
  }
 
   
  export function submit_suggest(data) {
    return request({
      url: 'message/submit_suggest',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
  
      },
      data: qs.stringify(data)
    })
  }
   
  export function get_user_suggest(data) {
    return request({
      url: 'message/get_user_suggest',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
  
      },
      data: qs.stringify(data)
    })
  }
   