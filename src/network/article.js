/*
 * @Author: yenible
 * @Date: 2021-01-14 10:33:02
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-16 10:14:08
 * @Description: xxx
 */
import qs from "qs";

import {
  request
} from './request'

export function save_article(userForm) {
  return request({
      url: 'article/save_article',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(userForm)
  })
}
// 删除文章
export function delete_article(userForm,isAdmin) {
  if(isAdmin){
    return request({
      url: 'article/delete_article',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(userForm)
  })
  }else{
    return request({
      url: 'article/delete_article',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(userForm)
  })
  }
  
}
export function query_user_article(form){
  return request({
    url: 'article/query_user_article',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(form)
  })
}
// 通过用户名查找文件
export function query_username_article(form){
  return request({
    url: 'article/query_username_article',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify(form)
  })
}
export function get_article_detail(userForm){
  return request({
    url: 'article/get_article_detail',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  });

}
export function get_article_info(userForm){
  return request({
    url: 'article/get_article_info',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 点赞操作
export function action_like(userForm){
  return request({
    url: 'article/action_like',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 查询当前文章是否已经点赞
export function article_is_like(userForm){
  return request({
    url: 'article/article_is_like',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}

export function action_collect(userForm){
  return request({
    url: 'article/action_collect',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 查询当前文章是否已经收藏
export function article_is_collect(userForm){
  return request({
    url: 'article/article_is_collect',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}

export function action_attract(userForm){
  return request({
    url: 'article/action_attract',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}

export function is_attract(userForm){
  return request({
    url: 'article/article_is_attract',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}

// 获取关注列表
export function get_attract_list(userForm){
  return request({
    url: 'article/get_attract_list',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}

// 获取粉丝列表
export function get_fan_list(userForm){
  return request({
    url: 'article/get_fan_list',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 取消关注
export function cancel_attract(userForm){
  return request({
    url: 'article/cancel_attract',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
export function get_user_info(id) {
  console.log(id)
  return request({
      url: 'article/get_user_info',
      method: 'get',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      params: {
          id:id
        }
  })
}
export function get_label_info(id) {
  return request({
      url: 'article/get_label_info',
      method: 'get',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      params:{article_id:id}
  })
}

export function get_article_type() {
  return request({
      url: 'article/get_article_type',
      method: 'get',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
  })
}
// 获取index的文章信息
export function get_article_sort(userForm){
  return request({
    url: 'article/get_article_sort',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify(userForm)
  })
}

export function query_collect_article(userForm) {
  return request({
      url: 'article/query_collect_article',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(userForm)
  })
}

export function create_label(userForm) {
  return request({
      url: 'article/create_label',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(userForm)
  })
}