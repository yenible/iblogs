/*
 * @Author: yenible
 * @Date: 2021-04-11 14:39:48
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-12 22:49:47
 * @Description: xxx
 */
/*
 * @Author: yenible
 * @Date: 2020-12-24 17:15:18
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-05 19:18:31
 * @Description: 用户数据请求的接口
 */
import qs from "qs";
import {
  request
} from './request'

export function get_chat_message(form) {
  return request({
      url: 'chat/get_chat_message',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data:  qs.stringify(form)
  })
}
 
export function get_recipient_info(form) {
  return request({
      url: 'chat/get_recipient_info',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data:  qs.stringify(form)
  })
}

export function get_chat_list(form) {
  return request({
      url: 'chat/get_chat_list',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data:  qs.stringify(form)
  })
}

export function remark_room(form) {
  return request({
      url: 'chat/remark_room',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data:  qs.stringify(form)
  })
}

 
export function clear_chat(form) {
  return request({
      url: 'chat/clear_chat',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data:  qs.stringify(form)
  })
}