/*
 * @Author: yenible
 * @Date: 2021-03-28 19:33:51
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-29 20:38:03
 * @Description: xxx
 */
/*
 * @Author: yenible
 * @Date: 2021-02-04 15:44:30
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-16 21:25:49
 * @Description: 上传图片、文件以及文章
 */
import qs from "qs";

import {
  request
} from './request'


export function search_my_myblogs(data) {
  return request({
      url: 'search/search_my_myblogs',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(data)
  })
}
// 搜索用户
export function search_user_name(data) {
  return request({
      url: 'search/search_user_name',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(data)
  })
}

// 全局搜索
export function search_blog_global(data) {
  return request({
      url: 'search/search_blog_global',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(data)
  })
}
 
export function search_file_global(data) {
  return request({
      url: 'search/search_file_global',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(data)
  })
}