/*
 * @Author: yenible
 * @Date: 2021-03-12 20:59:33
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-17 22:28:55
 * @Description: xxx
 */
import qs from "qs";

import {
  request
} from './request'

export function save_article_comment(form) {
  return request({
      url: 'comment/save_article_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(form)
    })
}

export function query_article_all_comment(form) {
  return request({
      url: 'comment/query_article_all_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(form)
    })
}


export function save_file_comment(form) {
  return request({
      url: 'comment/save_file_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(form)
    })
}

export function query_file_all_comment(form) {
  return request({
      url: 'comment/query_file_all_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(form)
    })
}
 
export function delete_comment(form,isAdmin) {
  if(isAdmin === 'admin'){
    return request({
      url: 'comment/delete_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(form)
    })
  }
  return request({
      url: 'comment/delete_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(form)
    })
}