/*
 * @Author: yenible
 * @Date: 2021-02-04 15:44:30
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-31 22:31:12
 * @Description: 上传图片、文件以及文章
 */
import qs from "qs";

import {
  request
} from './request'

export function upload_img(data) {
  return request({
      url: 'file/uploadImg',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: data
  })
}

export function move_article_img(data) {
  return request({
      url: 'file/move_article_img',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

      },
      data: qs.stringify(data)
  })
}

export function move_avatar(data) {
  return request({
      url: 'file/move_avatar',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

      },
      data: qs.stringify(data)
  })
}

export function get_user_info(id) {
  console.log(id)
  return request({
      url: 'article/get_user_info',
      method: 'get',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      params: {
          id:id
        }
  })
}
export function upload_file(regForm) {
  return request({
      url: 'file/upload_file',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: regForm
  })
}

export function query_file_list(data) {
  return request({
      url: 'file/query_file_list',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(data)
  })
}

export function query_file_edit(data) {
  return request({
      url: 'file/query_file_edit',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(data)
  })
}

export function update_file_info(data) {
  return request({
      url: 'file/update_file_info',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
      },
      data: qs.stringify(data)
  })
}
 
 