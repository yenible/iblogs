/*
 * @Author: yenible
 * @Date: 2021-03-18 15:34:11
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-31 22:32:13
 * @Description: xxx
 */
/*
 * @Author: yenible
 * @Date: 2021-02-04 15:44:30
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-16 21:25:49
 * @Description: 上传图片、文件以及文章
 */
import qs from "qs";

import {
  request
} from './request'



export function get_file_detail(data) {
  return request({
    url: 'file/get_file_detail',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify(data)
  })
}
export function get(data) {
  return request({
    url: 'file/get_file_detail',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')

    },
    data: qs.stringify(data)
  })
}


// 文件点赞
export function action_like_file(userForm) {
  return request({
    url: 'file/action_like_file',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 查询当前是否已经点赞
export function file_is_like(userForm) {
  return request({
    url: 'file/file_is_like',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 文件收藏
export function action_collect_file(userForm) {
  return request({
    url: 'file/action_collect_file',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
// 查询当前是否已经点赞
export function file_is_collect(userForm) {
  return request({
    url: 'file/file_is_collect',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}

export function download_file(userForm) {
  return request({
    url: 'file/download_file',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
    },
    data: qs.stringify(userForm)
  })
}
export function check_is_download(userForm) {
  return request({
    url: 'file/check_is_download',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify(userForm)
  })
}

// 获取用户下载中心列表
export function get_download_list(userForm) {
  return request({
    url: 'file/get_download_list',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')

    },
    data: qs.stringify(userForm)
  })
}

// 下载
//   export function download(file_id) {
//     return request({
//         url: 'file/download',
//         method: 'get',
//         headers: {
//             'Content-Type': 'application/x-www-form-urlencoded',
//       'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
//         },
//         params: {
//           file_id:file_id
//           }
//     })
// }

export function download(file_id) {
  return request({
    url: 'file/download',
    method: 'get',
    responseType: 'blob',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
    },
    params: {
      file_id: file_id
    }
  })

}
export function get_file_sort(userForm) {
  return request({
    url: 'file/get_file_sort',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: qs.stringify(userForm)
  })
}


export function query_collect_file(userForm) {
  return request({
    url: 'file/query_collect_file',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')

    },
    data: qs.stringify(userForm)
  })
}

export function delete_file(data, isAdmin) {
  console.log(isAdmin)
  if (isAdmin) {
    return request({
      url: 'file/delete_file',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('adminid')
      },
      data: qs.stringify(data)
    })
  } else {
    return request({
      url: 'file/delete_file',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': window.localStorage.getItem('token') + ' ' + window.localStorage.getItem('userid')
      },
      data: qs.stringify(data)
    })
  }

}