/*
 * @Author: yenible
 * @Date: 2021-03-30 21:31:08
 * @LastEditors: yenible
 * @LastEditTime: 2021-05-04 10:30:32
 * @Description: xxx
 */
import qs from "qs";

import {
  request
} from './request'

export function login(form) {
  return request({
      url: 'admin/login',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(form)
    })
}
// 确认当前用户的登录态是否正确
export function check_token() {
  return request({
      url: 'users/check_token',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
  })
}

// 确认当前用户的登录态是否正确
export function get_user_list(form) {
  return request({
      url: 'admin/get_user_list',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(form)
  })
}
 
export function update_user_info(form) {
  return request({
      url: 'admin/update_user_info',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(form)
  })
}
 
export function delete_user(form) {
  return request({
      url: 'admin/delete_user',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(form)
  })
}
search_blog_admin
export function search_blog_admin(form) {
  return request({
      url: 'admin/search_blog_admin',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(form)
  })
}


export function search_file_admin(form) {
  return request({
      url: 'admin/search_file_admin',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')
      },
      data: qs.stringify(form)
  })
}


export function update_password(form) {
  return request({
      url: 'users/update_password',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}

export function query_article_all_comment(form) {
  return request({
      url: 'comment/query_article_all_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}
 

export function query_file_all_comment(form) {
  return request({
      url: 'comment/query_file_all_comment',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}
 

export function get_suggest(form) {
  return request({
      url: 'admin/get_suggest',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}
 
export function reply_suggest(form) {
  return request({
      url: 'admin/reply_suggest',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}

export function get_admin_list(form) {
  return request({
      url: 'admin/get_admin_list',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}

export function create_admin(form) {
  return request({
      url: 'admin/create_admin',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}

export function delete_admin(form) {
  return request({
      url: 'admin/delete_admin',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}

export function update_admin_password(form) {
  return request({
      url: 'admin/update_admin_password',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('adminid')

      },
      data: qs.stringify(form)
  })
}