/*
 * @Author: yenible
 * @Date: 2020-12-24 17:15:18
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-05 19:18:31
 * @Description: 用户数据请求的接口
 */
import {
    request
} from './request'

export function login(userForm) {
    return request({
        url: 'users/login',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: userForm
    })
}

export function register(regForm) {
    return request({
        url: 'users/register',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: regForm
    })
}

export function update_info(regForm) {
    return request({
        url: 'users/update_info',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
        },
        data: regForm
    })
}

export function update_password(regForm) {
    return request({
        url: 'users/update_password',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')

        },
        data: regForm
    })
}

// 确认当前用户的登录态是否正确
export function check_token() {
    return request({
        url: 'users/check_token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
        },
    })
}

export function get_user_info(id) {
    // console.log(id)
    return request({
        url: 'users/get_user_info',
        method: 'get',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization' : window.localStorage.getItem('token') + ' ' +  window.localStorage.getItem('userid')
        },
        params: {
            id:id
          }
    })
}
// 通过名字获取用户基本信息
export function get_username_info(authname) {
    return request({
        url: 'users/get_username_info',
        method: 'get',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        params: {
            authname:authname
          }
    })
}
// 查询用户名是否存在
export function check_username(name) {
    console.log(name)
    return request({
        url: 'users/check_username',
        method: 'get',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        params: {
            name:name
          }
    })
}

// 查询邮箱是否已注册
export function check_email(email) {
    return request({
        url: 'users/check_email',
        method: 'get',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        params: {
            email:email
          }
    })
}