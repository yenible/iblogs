/*
 * @Author: yenible
 * @Date: 2020-12-23 11:59:12
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-18 10:38:48
 * @Description: xxx
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

import login from '@/views/login'
import profile from '@/views/profile/profile'
import edit from '@/views/edit/edit'
import myblogs from '@/views/myblogs/myblogs'
import index from '@/views/index/index'
import attract from '@/views/attract/attract'
import personalpage from '@/views/personalpage/personalpage'
import articledetail from '@/views/articledetail/articledetail'
import filedetail from '@/views/filedetail/filedetail'
import uploadfile from '@/views/uploadfile/uploadfile'
import collect from '@/views/collect/collect'
import download from '@/views/download/download'
import admin from '@/views/admin/admin'
import message from "@/views/message/message";
import chat from "@/views/chat/chat";
import suggest from "@/views/suggest/suggest";


import {
  get_user_info
} from "@/network/user.js";

Vue.use(VueRouter)

const routes = [{
    path: '/',
    // name: 'login',
    redirect: {
      name: 'index'
    }
  },
  {
    path: '/:userid/chat/:toUserName',
    name: 'chat',
    component: chat,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:userid/suggest',
    name: 'suggest',
    component: suggest,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/index',
    name: 'index',
    component: index,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:userid/message',
    name: 'message',
    component: message,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/:admin_id',
    name: 'admin',
    component: admin,
  },
  {
    path: '/admin/：adminid/usermanage',
    name: 'chat',
    component: chat,
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/:userid/profile',
    name: 'profile',
    component: profile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:userid/collect',
    name: 'collect',
    component: collect,
    meta: {
      requiresAuth: true
    }
  },
  
  {
    path: '/:userid/myblogs',
    name: 'myblogs',
    component: myblogs,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:userid/attract',
    name: 'attract',
    component: attract,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:userid/md/:articleid',
    name: 'md',
    component: edit,
    meta: {
      requiresAuth: true
    }
  }, 
  // {
  //   path: '/:userid/upload',
  //   name: 'uploadfile',
  //   component: uploadfile,
  //   children: [{
  //     path: 'edit/:file_id',
  //     name: 'edit',
  //     component: uploadfile,
  //   }],
  //   meta: {
  //     requiresAuth: true
  //   }
  // },
  {
    path: '/:userid/upload',
    name: 'uploadfile',
    component: uploadfile,
    meta: {
      requiresAuth: true
    }
  },
{
    path: '/:userid/upload/edit/:file_id',
    name: 'edit',
    component: uploadfile,
  },
  {
    path: '/:userid/download',
    name: 'download',
    component: download,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:authname/homepage',
    name: 'personalpage',
    component: personalpage,
    meta: {
      requiresAuth: false
    },
    // children: [
    //   {// UserProfile 会被渲染在 User 的 <router-view> 中
    //     path: 'blog/:article_id',
    //     component: articledetail
    //   },{
    //     path: 'test',
    //     component: test
    //   }
    // ]
  },

  {
    path: '/:authname/homepage/blog/:article_id',
    name: 'blogdetail',
    component: articledetail,
    meta: {
      requiresAuth: false
    },
  },
  {
    path: '/:authname/article/detail/:article_id',
    name: 'myblogdetail',
    component: articledetail,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/:authname/file/detail/:file_id',
    name: 'filedetail',
    component: filedetail,
    meta: {
      requiresAuth: false
    }
  },
]

const router = new VueRouter({
  routes,
  mode: 'history'
})
const routerPush = router.push;
router.push = path => {
  // 判断下当前路由是否就是要跳转的路由   
  if (router.currentRoute.fullPath.includes(path.path)) {
    return;
  }
  return routerPush.call(router, path);
}
import {
  check_token
} from "@/network/user";
router.beforeEach(async (to, from, next) => { // const tokenstr = window.sessionStorage.getItem('token')
  // 每次切回路由便请求一次用户数据？
  // let otoken = window.localStorage.getItem('token');
  // let ouserid = window.localStorage.getItem('userid');
  // if(otoken && ouserid){
  //   await getUserInfo(ouserid)
  // }
  // console.log(window.)
  // console.log(this)
  if (to.name !== 'Login' && to.matched.some(record => record.meta.requiresAuth)) {
    let isLogin;
    await check_token().then(result => {
      console.log(result.data)
      let res = result.data;
      if (res.errCode === 0 && res.token) {
        isLogin = true;
      } else {
        if (res.errObj && res.errObj === 'login') {
          isLogin = false;
        }
      }

    });
    if (!isLogin) {
      console.log(123)
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    } else {
      next()
    }
  } else {
    next()
  }

})


export default router