/*
 * @Author: yenible
 * @Date: 2021-03-09 17:15:26
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-11 13:28:25
 * @Description: xxx
 */
export default {
  userInfo: function (state) {
    return state.userInfo
  },
  isLogin: function (state) {
    return state.isLogin
  },
}
