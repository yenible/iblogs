/*
 * @Author: yenible
 * @Date: 2021-03-09 17:16:45
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-26 14:38:04
 * @Description: xxx
 */
import {
  UPADTE_USERINFO,
  UPADTE_ISLOGIN,
  UPADTE_ARTICLECOMMENTID,
  UPADTE_REFRSHCOMMENT,
  UPADTE_REFRSHUSERINFO,
  UPADTE_MYBLOGSTYPE
} from './mutation-type'

export default {
  // 更新保存用户数据
  [UPADTE_USERINFO] (state, userInfo) {
    state.userInfo = userInfo
  },
  [UPADTE_ISLOGIN] (state, isLogin) {
    state.isLogin = isLogin
  },
  [UPADTE_ARTICLECOMMENTID] (state, articleCommentId) {
    state.articleCommentId = articleCommentId
  },
  
  [UPADTE_REFRSHCOMMENT] (state, data) {
    state.refrshComment = data
  },
  [UPADTE_REFRSHUSERINFO] (state, data) {
    state.refrshUserInfo = data
  },
  [UPADTE_MYBLOGSTYPE] (state, data) {
    state.myblogsType = data
  },
}
