/*
 * @Author: yenible
 * @Date: 2021-03-25 21:50:19
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-25 21:52:05
 * @Description: xxx
 */

export function     windowDownload(filename, link) {
  let DownloadLink = document.createElement("a");
  DownloadLink.style = "display: none"; // 创建一个隐藏的a标签
  DownloadLink.download = filename;
  DownloadLink.href = link;
  document.body.appendChild(DownloadLink);
  DownloadLink.click(); // 触发a标签的click事件
  document.body.removeChild(DownloadLink);
}