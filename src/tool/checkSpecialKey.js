/*
 * @Author: yenible
 * @Date: 2021-04-04 21:31:57
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-04 21:32:08
 * @Description: xxx
 */
export function checkSpecificKey(str) {
  var specialKey = "[`~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'"; 
  for (var i = 0; i < str.length; i++) {
    if (specialKey.indexOf(str.substr(i, 1)) != -1) {
      return false;
    }
  }
  return true;
}