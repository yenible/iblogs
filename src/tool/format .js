/*
 * @Author: yenible
 * @Date: 2021-03-03 16:41:04
 * @LastEditors: yenible
 * @LastEditTime: 2021-03-03 16:44:04
 * @Description: 日期格式化函数
 */
export function format(datetime){ 

    var year = datetime.getFullYear();
    var month = datetime.getMonth()+1;//js从0开始取 
    var date = datetime.getDate(); 
    var hour = datetime.getHours(); 
    var minutes = datetime.getMinutes(); 
    var second = datetime.getSeconds();
    
    if(month<10){
     month = "0" + month;
    }
    if(date<10){
     date = "0" + date;
    }
    if(hour <10){
     hour = "0" + hour;
    }
    if(minutes <10){
     minutes = "0" + minutes;
    }
    if(second <10){
     second = "0" + second ;
    }
    
    var time = year+"-"+month+"-"+date+" "+hour+":"+minutes+":"+second; //2009-06-12 17:18:05
   // alert(time);
    return time;
   }