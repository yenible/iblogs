/*
 * @Author: yenible
 * @Date: 2020-12-23 11:59:12
 * @LastEditors: yenible
 * @LastEditTime: 2021-04-11 10:10:16
 * @Description: xxx
 */
import Vue from 'vue'
// import vueLazyLoad from 'vue-lazyload'
import App from './App.vue'
import router from './router'
import store from './store'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './plugins/element.js'
import VueClipboard from 'vue-clipboard2'

import scroll from 'vue-scroll'

 
import SocketIO from 'vue-socket.io'
import ClientSocketIO from 'socket.io-client'
// import VueSocketio from 'vue-socket.io';
// Vue.use(new VueSocketio('http://localhost:33335'));

 
// Vue.use(new SocketIO({
//   debug: true,
//   connection: 'http://localhost:33335',
//   options: { path: "/" }

// }))

Vue.use(new SocketIO({
  debug: true,
  connection: ClientSocketIO.connect('http://localhost:33335', {
    transports: ['websocket'],
    // autoConnect: false, // 关闭自动连接
  }),
}))



Vue.use(scroll)

Vue.use(VueClipboard)
Vue.config.productionTip = false
// 添加事件总线对象
// Vue.prototype.$bus = new Vue()
Vue.use(Element)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')